// javascript Document

function init() {
	var deletes = document.getElementsByClassName("pw");
	for (var i=0; i<2; i++) {
		deletes[i].value = "";
	}
}

$(function() {
	var dr_name = '氏名 (< 32 chars/16全角文字)';
	$('#dr_name')
		.addClass('watermark')
		.val(dr_name)
		.focus(function() {
			$(this).removeClass('watermark');
			if ($(this).val() === dr_name) {
				$(this).val('');
			}
		})
		.blur(function() {
			if ($(this).val() === '') {
				$(this).val(dr_name);
				$(this).addClass('watermark');
			}
		})
		.submit(function() {
			if ($(this).val() === dr_name) {
				$(this).val('');
			}
		});
});

$(function() {
	var sirname = 'Sirname (< 32 alphabetical chars)';
	$('#sirname')
		.addClass('watermark')
		.val(sirname)
		.focus(function() {
			$(this).removeClass('watermark');
			if ($(this).val() === sirname) {
				$(this).val('');
			}
		})
		.blur(function() {
			if ($(this).val() === '') {
				$(this).val(sirname);
				$(this).addClass('watermark');
			}
		})
		.submit(function() {
			if ($(this).val() === sirname) {
				$(this).val('');
			}
		});
});

$(function() {
	var firstname = 'Firstname (< 32 alphabetical chars)';
	$('#firstname')
		.addClass('watermark')
		.val(firstname)
		.focus(function() {
			$(this).removeClass('watermark');
			if ($(this).val() === firstname) {
				$(this).val('');
			}
		})
		.blur(function() {
			if ($(this).val() === '') {
				$(this).val(firstname);
				$(this).addClass('watermark');
			}
		})
		.submit(function() {
			if ($(this).val() === firstname) {
				$(this).val('');
			}
		});
});

$(function() {
	var email = 'aaa@bbb.ccc';
	$('#email')
		.addClass('watermark')
		.val(email)
		.focus(function() {
			$(this).removeClass('watermark');
			if ($(this).val() === email) {
				$(this).val('');
			}
		})
		.blur(function() {
			if ($(this).val() === '') {
				$(this).val(email);
				$(this).addClass('watermark');
			}
		})
		.submit(function() {
			if ($(this).val() === email) {
				$(this).val('');
			}
		});
});

$(function() {
	var hint = 'Your answer (will be encrypted)';
	$('#hint')
		.addClass('watermark')
		.val(hint)
		.focus(function() {
			$(this).removeClass('watermark');
			if ($(this).val() === hint) {
				$(this).val('');
			}
		})
		.blur(function() {
			if ($(this).val() === '') {
				$(this).val(hint);
				$(this).addClass('watermark');
			}
		})
		.submit(function() {
			if ($(this).val() === hint) {
				$(this).val('');
			}
		});
});

$(function() {
	$("#submit_btn").corner();
});
