﻿<?php		// member_registration01.php

	require_once('../utilities/config.php');
	require_once('../utilities/lib.php');	
	charSetUTF8();
	session_start();
	$today_year = date("Y");
	$_SESSION['mem_reg'] = hash("sha512", $today_year);
?>


<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="member_registration.css">
    <script src="../javascript/jquery-1.10.2.js"></script>
    <script src="../javascript/jquery-corner.js"></script>
    <script src="dr_registration.js"></script>

	<title>TRI Network</title>
	<script type="text/javascript">

/*********************************************************
	2014/09/02変更（はじめ）
*********************************************************/
/*
		function check(){
			if( $("select[name='job_kind']").val()==9 ){
				alert("この職種区分については主催者の承認が必要です。");
			}
		}
*/
/*********************************************************
	2014/09/02変更（おわり）
*********************************************************/
	</script>

</head>
<body onLoad="init()">

<h1>Registration of Yourself in KAMAKURA Live Demonstration</h1>
<div id="main">
<div id="alert">
  <p>Caution: Your password is stored in Database after encryptation. Thus, even the administrator cannot know your password.
    If you lose your password, your login ID will not be used anymore.    </p>
  <p>注意: あなたのパスワードはDatabase側でも暗号化して保存します。従いまして、管理者もそれが何かを知ることはできません。
    パスワードを紛失された場合にはそのログインIDは使用不能となります。 </p>
</div>

<form action="member_registration02.php" method="post">
<table>
    <tr>
    <td>Your Fullname : <br><small><font color='red'>(in Your Native Language)</font></small></td>
    <td><input type="text" id="dr_name" name="dr_name" size=27 maxlength=20 class="in" value="<?php
        	if (isset($_SESSION['dr_name'])) echo _Q($_SESSION['dr_name']);
		?>"></td></tr> 
    <tr>
    <td>Alphabetical Sirname (アルファベット姓) : </td>
    <td><input type="text" id="sirname" name="sirname" size=27 maxlength=20 class="in" value="<?php
        	if (isset($_SESSION['sirname'])) echo _Q($_SESSION['sirname']);
		?>"></td></tr> 
    <tr>
    <td>Alphabetical Firstname (アルファベット名) : </td>
    <td><input type="text" id="firstname" name="firstname" size=27 maxlength=20 class="in" value="<?php
        	if (isset($_SESSION['firstname'])) echo _Q($_SESSION['firstname']);
		?>"></td></tr> 
    <tr></tr>    
 	<tr>
 	  <td>Your Sex (性別) : </td><td>
  	<input type="radio" name="is_male" value="1"
    	<?php
        	if (!isset($_SESSION['is_male'])||($_SESSION['is_male'] == 1)) echo "checked";
		?>>
  	<span id="male">Male (男性)</span>
    <input type="radio" name="is_male" value="2"
    	<?php
        	if (isset($_SESSION['is_male'])&&($_SESSION['is_male'] == 2)) echo "checked";
		?>>
    <span id="female">Female (女性)</span></td></tr>

	<tr>
	  <td>Your Birth Year (西暦生まれ年) : </td><td>
    <select id="birth_year" name="birth_year" size="1">
    	<?php
		if (isset($_SESSION['birth_year'])) {
			$target_year = $_SESSION['birth_year'];
		} else {
			$target_year = $today_year - 40;
		}
		for($i=1945; $i<($today_year-20); $i++) {
    		echo "<option value='".$i."'";
				if ($i==$target_year) echo " selected";
			echo ">".$i."</option>";
		}
		?>
        </select>
    </td></tr>
	<tr>
    <td>Your Affiliation (ご所属) : <br /></td>
    <td><textarea id="hp_name" name="hp_name" rows="3" cols="27" class="hp"><?php if (isset($_SESSION['hp_name'])) echo _Q($_SESSION['hp_name']) ?></textarea></td></tr>
    
    	<tr>
    <td>Address of Affiliation (所属先住所) : <br /></td>
    <td><textarea id="hp_address" name="hp_address" rows="3" cols="27" class="hp"><?php if (isset($_SESSION['hp_address'])) echo _Q($_SESSION['hp_address']) ?></textarea></td></tr>
    
   	  <tr>
    <td>Your Country (国籍) :  </td>
    <td><select id="country_code" name="country_code">
    <?php
		foreach($country_codes as $country_code => $country_name) {
    		echo "<option value=$country_code ";
			if (isset($_SESSION['country_code'])&&($country_code == $_SESSION['country_code'])) echo "selected";
			echo ">$country_name</option>";
		}
	?>
        </select>
    </td></tr>
    
	<tr>
    <td>Your Specialty (ご専門) :  </td>
<!--
/*********************************************************
	2014/08/29変更（はじめ）
*********************************************************/
    <td><select id="job_kind" name="job_kind">
-->
    <td><select id="job_kind" name="job_kind">
<!--
/*********************************************************
	2014/08/29変更（おわり）
*********************************************************/
-->
    <?php
		foreach($job_kinds as $job_kind => $job_name) {
    		echo "<option value=$job_kind ";
			if (isset($_SESSION['job_kind'])&&($job_kind == $_SESSION['job_kind'])) echo "selected";
			echo ">$job_name</option>";
		}
	?>
        </select>
    </td></tr>
	<tr></tr>
	<tr>
<td>Your E-mail <small><font color="red"> (= login ID)</font></small> : </td>
<td><input class="mail" oncopy="return false" onpaste="return false" oncontextmenu="return false"  type="text" name="email" id="email" size=64 maxlength=128></td></tr>
<tr>
  <td>Re-type Your E-mail (再入力): </td><td><input class="mail" oncopy="return false" onpaste="return false" oncontextmenu="return false"  type="text" name="email2" size=64 maxlength=128></td></tr>
	<tr></tr>
	<tr>
	  <td>Your Password (パスワード): </td><td>
    <input class="pw" type="password" id="pw" name="dr_pwd" size=30 maxlength=32></td></tr>
	<tr>
	  <td>Re-type Password (再入力) : </td><td>
    <input class="pw" type="password" name="dr_pwd2" size=30 maxlength=32></td></tr>
	<tr></tr>
	<tr>
    <td><p>Hint when forgetting password (パスワード紛失時ヒント) :<br />
    </p></td>
    <td><select id="clue" name="clue">
    <?php
		foreach($hints as $clue => $hint_item) {
    		echo "<option value=$clue>$hint_item</option>";
		}
	?>
        </select></td></tr>
   <tr>
   <td>Your Answer for Hint (その問に対するあなたの回答) : </td>
   <td><input type="text" id="hint" name="hint" size="40" maxlength="30" class="in" /></td></tr>    
	<tr></tr>
<tr><td colspan="2" style="text-align:center"><input type="submit" value="  Push This Button to Confirm  (確認)" id="submit_btn"></td></tr>
</table>
</form>

</div>

</body>
</html>
