<?php
/*********************************************************
	2014/09/03 Officials用の登録　実際には事務局にメール発信するのみ
*********************************************************/
	require_once("../../utilities/config.php");
	require_once("../../utilities/lib.php");	
	charSetUTF8();
	session_start();

	if (!isset($_SESSION['index_key'])||($_SESSION['index_key'] != hash("sha512", $magic_code))) {
		$_SESSION = array();
		header('Location: ../../index.php');
	}


if (!isset($_POST['dr_tbl_id'])||!is_numeric($_POST['dr_tbl_id'])||!isset($_POST['conf_tbl_id'])||!is_numeric($_POST['conf_tbl_id'])||!isset($_POST['job_kind'])||!is_numeric($_POST['job_kind'])) {
		echo "<body bgcolor='red'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	} else {
		$conf_tbl_id = $_POST['conf_tbl_id'];
		$dr_tbl_id = $_POST['dr_tbl_id'];
		$job_kind = $_POST['job_kind'];
	}

/*
	if (!isset($_POST['conf_link_tbl_id'])||!is_numeric($_POST['conf_link_tbl_id'])) {
	if (!isset($_POST['conf_link_tbl_id'])) {
		echo "<body bgcolor='red'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	} else {
		$conf_link_tbl_id = $_POST['conf_link_tbl_id'];
	}	
*/

	if (isset($_SESSION['error_msg'])) {
		echo "<script type='text/javascript'>";
		echo "alert('".$_SESSION['error_msg']."');";
		echo "</script>";
	}
	
	$_SESSION['can_go_payment01'] = true;
?>

	
<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">


<title>NPO Registration</title>
    <!-- Bootstrap core CSS -->
	<link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.css">
 
 <!-- Custom styles for this template -->
	<link rel="stylesheet" type="text/css" href="../../bootstrap/jumbotron/jumbotron.css">


</head>

<body>
      <h1 class="alert-success">&nbsp;&nbsp;Welcome Officials/Authorities!</h1>
 
<div class="jumbotron"  style="background-color:rgba(170,156,3,1.00); padding-top:25px; padding-bottom:5px;">
      <div class="container">
        <p  class="white">あなた宛てにメールを送信させて頂きましたので、迷惑メールフォルダも含め、ご確認下さい</p><br>
        <p class="alert-warning">鎌倉ライブデモンストレーション2014に事前登録ありがとうございます</p>
		<p class="alert-warning">あなたは、Officials/Authrities区分で登録されました</p>
		<p class="alert-warning">あなたの参加料は当日身分証明書を持参して頂ければ免除されます</p>
        <br>
 <p><a href="../../index.php" class="btn btn-danger btn-lg" role="button" id="program2014">トップページに戻る &raquo;</a></p>
  </div>
</div>

      <hr>

      <footer>
        <p>&copy; 2013 - 2014 by NPO TRI International Network</p>
      </footer>
    </div> <!-- /container -->
  
  <?php	
/************************************************************
*** ここからメール発信
*************************************************************/
//接続
 	try {
    // MySQLサーバへ接続
				$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
				$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);	// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる

	} catch(PDOException $e){
    	die($e->getMessage());
	}
	
	$stmt = $pdo->prepare("SELECT * FROM `dr_tbl` WHERE `id` = :dr_tbl_id");
	$stmt->bindValue(":dr_tbl_id", $dr_tbl_id);
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	$subject = "${site_name} 登録完了通知メール";
	$sender = mb_encode_mimeheader("NPO TRI International Network");
	$headers  = "FROM: ".$sender."<$support_mail>\r\n";
	$headers .= "Bcc:transradial@kamakuraheart.org, rie_iwamoto@kamakuraheart.org, kaori_sudo@kamakuraheart.org";
	
	//　ここからメール内容
	$body = <<< _EOT_
Dear Mr/Ms/Dr {$row['dr_name']}:
	
鎌倉ライブデモンストレーション2014に事前登録ありがとうございます
あなたは、Officials/Authrities区分で登録されました
あなたの参加料は当日身分証明書を持参して頂ければ免除されます

ご質問やご意見ありますれば　{$support_mail} まで
お問い合わせ下さい。
===========================================================
	$site_name
	$site_url
_EOT_;
	// _EOT_　は行の最初に記述し、;の後にコメントも無し、にしないとエラーとなる	

		if ($_SERVER['SERVER_NAME'] === 'localhost') {
			echo "<br>".$body."<br>";
		} else {
			mb_language('uni');
			mb_internal_encoding('utf-8');
			mb_send_mail($row['email'], $subject, $body, $headers, "-f$support_mail"); 
		}


		$_SESSION = array();
		session_destroy();
		exit;
?>  
    
    <!-- Bootstrap core javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script> -->
<?php
	if ($_SERVER['HTTP_HOST'] == 'localhost') {
		echo '<script src="../../javascript/jquery-1.10.2.js"></script>';
	} else {
		echo '<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>';
	}
?>
	<script src="../../javascript/jquery-corner.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <script src="live2014.js"></script>

</body>
</html>
