﻿<?php
	require_once("../../utilities/config.php");
	require_once("../../utilities/lib.php");	
	charSetUTF8();
	session_start();

	if (!isset($_SESSION['index_key'])||($_SESSION['index_key'] != hash("sha512", $magic_code))) {
		$_SESSION = array();
		header('Location: ../../index.php');
	}

/*********************************************************
	2014/07/02変更（はじめ）
*********************************************************/
//	if (!isset($_POST['dr_tbl_id'])||!is_numeric($_POST['dr_tbl_id'])||!isset($_POST['conf_tbl_id'])||!is_numeric($_POST['conf_tbl_id'])) {
	if (!isset($_POST['dr_tbl_id'])||!is_numeric($_POST['dr_tbl_id'])||!isset($_POST['conf_tbl_id'])||!is_numeric($_POST['conf_tbl_id'])||!isset($_POST['job_kind'])||!is_numeric($_POST['job_kind'])) {
		echo "<body bgcolor='red'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	} else {
		$conf_tbl_id = $_POST['conf_tbl_id'];
		$dr_tbl_id = $_POST['dr_tbl_id'];
		$job_kind = $_POST['job_kind'];
	}
/*
	if (!isset($_POST['conf_link_tbl_id'])||!is_numeric($_POST['conf_link_tbl_id'])) {
		echo "<body bgcolor='red'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	} else {
		$conf_link_tbl_id = $_POST['conf_link_tbl_id'];
	}	
	
	if ($_POST['conf_tbl_id'] != 1) {		// きちんと鎌倉ライブ2014の入金であるか否かチェック
		$error = "Illegal access to KAMAKURA LIVE 2014 Payment Page!";
		echo $error;
		exit();
	}
*/
/*********************************************************
	2014/07/02変更（終わり）
*********************************************************/

	if (!isset($_SESSION['can_go_payment01'])||!$_SESSION['can_go_payment01']) {		// 不正なページからのアクセス
		$_SESSION = array();
		session_destroy();
		header('Location: ../../index.php');
	}	
	
	$_SESSION['can_go_gmo_transaction'] = true;


/*********************************************************
	2014/07/02変更（はじめ）
*********************************************************/
/*
//接続
 	try {
    // MySQLサーバへ接続
   	$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
	// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
	} catch(PDOException $e){
    	die($e->getMessage());
	}

	$stmt = $pdo->prepare("SELECT * FROM `dr_tbl` WHERE `id` = :dr_tbl_id;");
	$stmt->bindValue(":dr_tbl_id", $dr_tbl_id);
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	$job_kind = $row['job_kind'];
*/
	if ($job_kind!=2 && $job_kind!=3 && $job_kind!=4 && $job_kind!=5 && $job_kind!=6 && $job_kind!=7 && $job_kind!=8 && $job_kind!=9) {	// コメディカルであることを確認し、違えば戻る
		$error = "あなたはコメディカルでないので、この支払はできません!";
		$_SESSION['error_msg'] = $error;
		header("location: payment00.php");
		exit();
	}
/*********************************************************
	2014/07/02変更（終わり）
*********************************************************/

?>	
	
	
<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">


<title>NPO Registration</title>
    <!-- Bootstrap core CSS -->
	<link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.css">
 
 <!-- Custom styles for this template -->
	<link rel="stylesheet" type="text/css" href="../../bootstrap/jumbotron/jumbotron.css">


</head>

<body>
      <h1 class="alert-success">&hearts;&nbsp;Welcome to Co-Medical Payment System&nbsp;&hearts;</h1>
 
<div class="jumbotron"  style="background-color:blue;; padding-top:25px; padding-bottom:5px;">
      <div class="container">
        <p  class="white">カード決済に関しては、カード決済運用会社GMOにより行われます。特定非営利活動法人ティー・アール・アイ国際ネットワークではあなたのカード番号などを知ることはできず、決済されたか否かという情報のみ知ることになります。<br>
        Card settlement is done through the professional company "GMO". NPO TRI International Network has no access to your card number. NPO can know only the result of the settlement.<br><br></p>
<!--
/*********************************************************
	2014/07/02変更（はじめ）
*********************************************************/
 <form method="post" action="gmo/gmo_transaction00.php" id="form_to_GMO">
-->
 <form method="post" action="../gmo/gmo_transaction00.php" id="form_to_GMO">
<!--
/*********************************************************
	2014/07/02変更（終わり）
*********************************************************/
-->
 	<input type="hidden" name="dr_tbl_id" value="<?= $dr_tbl_id ?>" >
    <input type="hidden" name="conf_tbl_id" value="<?= $conf_tbl_id ?>">
    <input type="hidden" name="conf_link_tbl_id" value="<?= $conf_link_tbl_id ?>">
 <p class="text-center"><a class="btn btn-success btn-lg" role="button" id="gotoGMO">カード決済に進む &raquo;</a></p>
 <p class="text-center"><a class="btn btn-warning btn-lg" role="button" href="../../index.php" id="goto_index">取りやめる &raquo;</a></p>
</form>
  </div>
</div>

      <footer>
        <p>&copy; 2013 - 2014 by NPO TRI International Network</p>
      </footer>
    </div> <!-- /container -->
    
    
    <!-- Bootstrap core javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script> -->
<?php
	if ($_SERVER['HTTP_HOST'] == 'localhost') {
		echo '<script src="../../javascript/jquery-1.10.2.js"></script>';
	} else {
		echo '<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>';
	}
?>
	<script src="../../javascript/jquery-corner.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <script src="live2014.js"></script>

</body>
</html>
