<?php
	require_once('../../utilities/config.php');
	require_once('../../utilities/lib.php');	
	charSetUTF8();
//	session_set_cookie_params(0, "/", "/member/", TRUE, TRUE);
	session_start();
	$_SESSION['last_time'] = time();	// session timeoutのための変数
//	$_SESSION = array();
	$_SESSION['index_key'] = hash("sha512", $magic_code);
?>

<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<link rel="stylesheet" type="text/css" href="../../css/index.css"/>
 <script src="../../javascript/jquery-1.10.2.js"></script>
<script src="../../javascript/jquery-corner.js"></script>
<script src="../../javascript/index.js"></script>
<title>NPO Registration</title>
</head>

<body>
<?php
	if (!auth_dr()) {
		header("Location:../../index.php");
		exit();
	}
?>
<div id="title">
NPOティー・アール・アイ国際ネットワーク演題登録システム
<div id="eng_title">Presentation Registration System of NPO TRI International Network</div>
</div>
<div class="center">

<p class="welcome">Welcome Mr/Ms <?= _Q($_SESSION['dr_name_alpha']); ?>　　</p>

    
<?php

//接続
 		try {
    	// MySQLサーバへ接続
   		$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
		// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
		} catch(PDOException $e){
    		die($e->getMessage());
		}
		$stmt = $pdo->prepare("SELECT * FROM `conf_link_tbl` INNER JOIN `conf_tbl` ON `conf_link_tbl`.`conf_tbl_id` = ".
			"`conf_tbl`.`id` WHERE `conf_link_tbl`.`dr_tbl_id` = :dr_tbl_id AND DATEDIFF(`begin` , CURRENT_DATE( )) > :abstract_deadline;");
		$stmt->bindValue(":dr_tbl_id", $_SESSION['dr_tbl_id']);
		$stmt->bindValue(":abstract_deadline", $abstract_deadline);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if ($stmt->rowCount()>0) {	// rowCount() conf_link_tbl
?>
 
    
<!--           ここから学会演題登録			------->  
<br><br><br>
	<h3 class="index_table2">You can submit abstract in the following meeting(s).<br>
    以下の会で演題登録が可能です</h3>
    <table class="registered_table">
    <tr><th class="registered_table">Date</th><th class="registered_table">Conference Name</th><th class="registered_table">Abstract (演題登録)</th>
    </tr>
<?php
			foreach($rows as $value) {
				if (isset($_SESSION['role_kind'])) unset($_SESSION['role_kind']);
				if (isset($_SESSION['topic_title'])) unset($_SESSION['topic_title']);
				if (isset($_SESSION['topic_abstract'])) unset($_SESSION['topic_abstract']);
?>
<!--           ここから演題登録 			------->    
	<tr>
    <td class="registered_table"><?= _Q($value['begin']) ?></td>
    <td class="registered_table"><?= _Q($value['conf_ename']) ?></td>	
    <td class="registered_table">	
		<form action="topic02.php" method="post">
        	<input type="submit" value="Abstract Submission (演題登録)" class="submit_index">
           	<input type="hidden" name="conf_link_tbl_id" value="<?= _Q($value['id']) ?>">
           	<input type="hidden" name="conf_tbl_id" value="<?= _Q($value['conf_tbl_id']) ?>">
            <input type="hidden" name="dr_tbl_id" value="<?= _Q($_SESSION['dr_tbl_id']) ?>">
		</form></td>					
    </tr>

<?php
			}	// foreach
?>
    </table>
    
<?php
			$stmt = $pdo->prepare("SELECT * FROM `role_tbl` WHERE `role_tbl`.`dr_tbl_id` = :dr_tbl_id;");

			$stmt->bindValue(":dr_tbl_id", $_SESSION['dr_tbl_id']);
			$stmt->execute();
			$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

			if ($stmt->rowCount()>0) {	// rowCount() role_tbl
?>
				<h3 id="abstract_table">Your current role/abstract(s) is listed bellow.<br />あなたの現在の演題/役割は以下の通りです</h3>;
				<table class="abstract_table"><tr>
				<th class="abstract_table">TITLE</th><th class="abstract_table">ROLE</th>
				<th class="abstract_table">View/Modify Astract<br />(あなたの演題)</th></tr>
<?php
				foreach($rows as $value) {

?>
					<tr><td class="abstract_table"><?= _Q($value['topic_title']) ?></td>
					<td class="abstract_table"><?= $role_kinds[$value['role_kind']] ?></td>
<?php
					if (($value['role_kind']==1)||($value['role_kind']==2)) {
?>
						<td class="abstract_table">
                        	<form action="view01.php" method="post">
                                <input type="hidden" name="role_tbl_id" value="<?= _Q($value['id']) ?>" />
                                <input type="submit" value="View/Modify (あなたの提出演題)" />
                            </form>
                        </td>
<?php
					} else {
?>
                        <td class="abstract_table"><br /></td>
<?php
					}	
				}	// foreach
?>
				</table>
<?php
			} 	// rowCount() role_tbl
		}	// rowCount() conf_link_tbl
?>

    <br /><br />

	<button class="logout" id="logout">Logout (ログアウト)</button>


</div>
</body>
</html>
