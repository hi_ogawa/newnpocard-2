<?php		// view03.php

	require_once('../../utilities/config.php');
	require_once('../../utilities/lib.php');	
	charSetUTF8();
	session_start();
	if (!isset($_SESSION['index_key'])||($_SESSION['index_key'] != hash("sha512", $magic_code))) {
		$_SESSION = array();
		header('Location: ../../index.php');
	}
	if (auth_dr()) {
		//接続
 		try {
    	// MySQLサーバへ接続
   		$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
		// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
		} catch(PDOException $e){
    		die($e->getMessage());
		}
		$stmt = $pdo->prepare("UPDATE `role_tbl` SET `topic_title` = :topic_title,  `topic_abstract` = :topic_abstract WHERE `id` = :role_tbl_id;");
		$stmt->bindValue(":role_tbl_id", $_POST['role_tbl_id']);
		$stmt->bindValue(":topic_title", $_POST['topic_title']);
		$stmt->bindValue(":topic_abstract", $_POST['topic_abstract']);
		$stmt->execute();
	} else {
		$_SESSION = array();

	}
	header('Location: ../../index.php');	
?>
