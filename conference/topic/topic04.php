<?php		// topic04.php

	require_once('../../utilities/config.php');
	require_once('../../utilities/lib.php');	
	charSetUTF8();
	session_start();
	if (!isset($_SESSION['index_key'])||($_SESSION['index_key'] != hash("sha512", $magic_code))) {
		$_SESSION = array();
		header('Location: ../../index.php');
	}

	if (!isset($_POST['dr_tbl_id'])||!is_numeric($_POST['dr_tbl_id'])||!isset($_POST['conf_tbl_id'])||!is_numeric($_POST['conf_tbl_id'])) {
		echo "<body bgcolor='red'>";
		echo "<h1 align='center'><font color='white'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	} else {
		$conf_tbl_id = $_POST['conf_tbl_id'];
		$dr_tbl_id = $_POST['dr_tbl_id'];
	}
	
	if (auth_dr()) {
		//接続
 		try {
    	// MySQLサーバへ接続
   		$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
		// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
		} catch(PDOException $e){
    		die($e->getMessage());
		}
		$stmt = $pdo->prepare("INSERT INTO `role_tbl` (`conf_tbl_id`, `dr_tbl_id`, `role_kind`, `topic_title`, `topic_abstract`) VALUES ".
			"(:conf_tbl_id, :dr_tbl_id, :role_kind, :topic_title, :topic_abstract);");
		$stmt->bindValue(":conf_tbl_id", $_POST['conf_tbl_id']);
		$stmt->bindValue(":dr_tbl_id", $_POST['dr_tbl_id']);
		$stmt->bindValue(":role_kind", $_POST['role_kind']);
		$stmt->bindValue(":topic_title", $_POST['topic_title']);
		$stmt->bindValue(":topic_abstract", $_POST['topic_abstract']);
		$stmt->execute();
	} else {
		$_SESSION = array();

	}
	header('Location: ../../index.php');	
?>
