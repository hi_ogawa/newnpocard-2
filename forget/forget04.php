<?php		// forget04.php

 //
	require_once('../utilities/config.php');
	require_once('../utilities/lib.php');	
	charSetUTF8();
	//	session_set_cookie_params(0, "/", "/member/", TRUE, TRUE);		
	session_start();
//接続
 	try {
    // MySQLサーバへ接続
   	$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
	// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
	} catch(PDOException $e){
    	die($e->getMessage());
	}

	$stmt = $pdo->prepare("SELECT * FROM `dr_tbl` WHERE `email` = :email AND `is_active` = '1' AND `is_usable` = '1';");
	$stmt->bindValue(":email", $_GET['email']);
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
//	echo "md5 = ".substr(hash("sha512", $_GET['email'].$magic_code.$row['modified']), 10, 32)."<br>hint = ".substr($row['hint'], 10, 32)."<br>";

	if ($stmt->rowCount() == 1) { // このメルアドのアクティブ・ユーザーが登録されている
		if (($_GET['md5'] == substr(hash("sha512", $_GET['email'].$magic_code.$row['modified']), 10, 32))&&
			($_GET['hint'] == substr($row['hint'], 10, 32))) {	// 正しく本人がパスワード変更を要求している
			if ((strtotime(date('Y-m-d H:i:s')) - strtotime($row['modified'])) < 60*10) {	// 変更要求から10分以内ならばOK
				$_SESSION['auth_forget'] = hash("sha512", $magic_code);
				$_SESSION['email'] = $row['email'];
				$_SESSION['dr_tbl_id'] = $row['id'];
				$_SESSION['dr_pwd'] = $row['dr_pwd'];
				$_SESSION['dr_name'] = $row['dr_name'];
				$_SESSION['sirname'] = $row['sirname'];	
				$_SESSION['firstname'] = $row['firstname'];
				$_SESSION['hp_name'] = $row['hp_name'];	
				header('Location: forget05.php');
				exit();
			} else {

?>

<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<link rel="stylesheet" type="text/css" href="css/index.css"/>
 <script src="../javascript/jquery-1.10.2.js"></script>
<script src="../javascript/jquery-corner.js"></script>
<script src="../javascript/index.js"></script>
    <link rel="stylesheet" type="text/css" href="../mem_reg/member_reg_mail.css">
	<title>NPO TRI</title>
</head>
<body>
<h1><br/>Request for Password Change<br/></h1>
<div align="center">
<table width="80%">
<tr><td class="r">
あなたの電子メール・アドレスからパスワード変更要求が行われました<br />
しかしながら、その要求から時間が経過したため、パスワードは変更されません</td></tr>
<tr><td class="r">
The system has received a request for password change from your email address.<br />
However, the request was rejected due to dalayed access.</td></tr>
<tr><td class="b">** 変更要求から10分以内に変更して下さい。</td></tr>
<tr><td class="b">** You have to finish the process within 10 minutes after requesting.</td></tr>
</table>
</div>
</bodY>
</html>
<?php
			}
		}
	}
?>

